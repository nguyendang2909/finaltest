const express = require('express');

const {getQuote} = require('./function.js')
const app = express();
const convert = require('convert-units');
const { MongoClient } = require('mongodb');

const url = 'mongodb://localhost:27017/';
let pricevalue = 0;
app.use(express.json());


app.use((request, response, next) => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  response.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
  response.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

  next();
});


// Get Quote
app.post('/getQuote', (req, res) => {
    // Origin country, destination country, weight fields are required (weight >=0)
  if (req.body.weight <= 0
    || Number.isNaN(req.body.weight)
    || req.body.sendercountry === ''
    || req.body.receivercountry === '') {
    res.send({ statusGetQuote: false });
  } else {
    let weight = convert(req.body.weight).from('kg').to('g'); // Convert weight from kg to g
    
    MongoClient.connect(url)
      .then((db) => {
        const myobj = {
          sendername: `${req.body.sendername}`,
          senderemail: `${req.body.senderemail}`,
          senderphone: `${req.body.senderphone}`,
          senderadress: `${req.body.senderadress}`,
          senderlocality: `${req.body.senderlocality}`,
          senderpostalcode: `${req.body.senderpostalcode}`,
          sendercountry: `${req.body.sendercountry}`,
          receivername: `${req.body.receivername}`,
          receiveremail: `${req.body.receiveremail}`,
          receiverphone: `${req.body.receiverphone}`,
          receiveradress: `${req.body.receiveradress}`,
          receiverlocality: `${req.body.receiverlocality}`,
          receiverpostalcode: `${req.body.receiverpostalcode}`,
          receivercountry: `${req.body.receivercountry}`,
          receiverlength: `${req.body.receiverlength}`,
          receiverwidth: `${req.body.receiverwidth}`,
          receiverheight: `${req.body.receiverheight}`,
          weight: `${req.body.weight}`,
        };

        // Write customer information into the database
        db.db('ship').collection('user').insert(myobj).then(() => {
          // console.log(`Number of documents inserted: ${res.insertedCount}`);
        });
        // Find the price based on "rate" table
        db.db('ship').collection('rate').findOne({
          weight: { $gte: req.body.weight },
          from: req.body.sendercountry,
          to: req.body.receivercountry,
        })
          .then((result) => {
            if (!result) {
              res.send({ statusGetQuote: false }); // Notify re-entered if result is null
            } else {
              pricevalue = result.price;
              res.send({ giatien: result.price, statusGetQuote: true });
            }
            // console.log(result.weight);
          });
      })
      .catch((err) => {
        if (err) throw err;
      });
  }
});


// CreateShipment
app.post('/createShipment', (req, shipment) => {
  const ref = Math.floor((Math.random() * 10000000000)); // Random number in 10 character

  // Write shipment information in to the database
  const shipmentobj = {
    ref: `${ref}`,
    createdat: `${new Date().getDate()}/${new Date().getMonth()}/${new Date().getFullYear()} ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`,
    cost: `${pricevalue}`,
    sendername: `${req.body.sendername}`,
    senderemail: `${req.body.senderemail}`,
    senderphone: `${req.body.senderphone}`,
    senderadress: `${req.body.senderadress}`,
    senderlocality: `${req.body.senderlocality}`,
    senderpostalcode: `${req.body.senderpostalcode}`,
    sendercountry: `${req.body.sendercountry}`,
    receivername: `${req.body.receivername}`,
    receiveremail: `${req.body.receiveremail}`,
    receiverphone: `${req.body.receiverphone}`,
    receiveradress: `${req.body.receiveradress}`,
    receiverlocality: `${req.body.receiverlocality}`,
    receiverpostalcode: `${req.body.receiverpostalcode}`,
    receivercountry: `${req.body.receivercountry}`,
    receiverlength: `${req.body.receiverlength}`,
    receiverwidth: `${req.body.receiverwidth}`,
    receiverheight: `${req.body.receiverheight}`,
    weight: `${req.body.weight}`,

  };
  MongoClient.connect(url)
    .then((db) => {
      db.db('ship').collection('shipment').insert(shipmentobj)
        .then(() => {
          // console.log(`Number of documents inserted: ${res.insertedCount}`);
        });

      shipment.send({ ref });
    })
    .catch((err) => {
      if (err) throw err;
    });
});


// GetShipment
app.post('/getShip', (req, res) => {
  // console.log(req.body);
  MongoClient.connect(url)
    .then((db) => {
      db.db('ship').collection('shipment').findOne({ ref: req.body.trackref })
        .then((result) => {
          if (!result) { return res.send({ statusGetShipment: false }); }

          res.send({ resultGetShip: result, statusGetShipment: true });
          // console.log(result.sendername);
          return result;
        });
    })
    .catch((err) => {
      if (err) throw err;
    });
});

// DeleteShipment
app.post('/deleteShip', (req, res) => {
  // console.log(req.body);
  MongoClient.connect(url)
    .then((db) => {
      db.db('ship').collection('shipment').deleteOne({ ref: req.body.trackref })
        .then((result) => {
          res.send(result);
          // console.log(result.sendername);
        });
    })
    .catch((err) => {
      if (err) throw err;
    });
});


app.listen(1234, () => {
  //  console.log('Running')
});
