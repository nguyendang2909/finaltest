const {MongoClient} = require('mongodb');
const url = 'mongodb://localhost:27017/';

function getQuote(sendername,senderemail,senderphone,senderadress,senderlocality,senderpostalcode,
  sendercountry,receivername,receiveremail,receiverphone,receiveradress,receiverlocality,receiverpostalcode,receivercountry
  ,receiverlength,receiverwidth,receiverheight,weight) {
    if (weight <= 0) throw new Error('Invalid Weight')
   
  return new Promise((resolve,reject) =>{
    MongoClient.connect(url)
    .then((db) => {
      const myobj = {
        sendername: `${sendername}`,
        senderemail: `${senderemail}`,
        senderphone: `${senderphone}`,
        senderadress: `${senderadress}`,
        senderlocality: `${senderlocality}`,
        senderpostalcode: `${senderpostalcode}`,
        sendercountry: `${sendercountry}`,
        receivername: `${receivername}`,
        receiveremail: `${receiveremail}`,
        receiverphone: `${receiverphone}`,
        receiveradress: `${receiveradress}`,
        receiverlocality: `${receiverlocality}`,
        receiverpostalcode: `${receiverpostalcode}`,
        receivercountry: `${receivercountry}`,
        receiverlength: `${receiverlength}`,
        receiverwidth: `${receiverwidth}`,
        receiverheight: `${receiverheight}`,
        weight: `${weight}`,
      };

      // Write customer information into the database
      db.db('ship').collection('user').insert(myobj).then(() => {
        // console.log(`Number of documents inserted: ${res.insertedCount}`);
      });
      // Find the price based on "rate" table
      db.db('ship').collection('rate').findOne({
        weight: { $gte: weight },
        from: sendercountry,
        to: receivercountry,
      })
        .then((result) => {
          if (!result) {
            res.send({ statusGetQuote: false }); // Notify re-entered if result is null
          } else {
            pricevalue = result.price;
            res.send({ giatien: result.price, statusGetQuote: true });
          }
          // console.log(result.weight);
        });
    })
    .catch((err) => {
      if (err) throw err;
    });
  
  })  
    
      
  }