const express = require('express');
const app = express();
const convert = require('convert-units');
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/"
let pricevalue = 0
app.use(express.json());


app.use((request, response, next) => {
	response.setHeader('Access-Control-Allow-Origin', '*'); 
	response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
	response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

	next();
});


//Get Quote
app.post('/getQuote', (req,res)=>{
    
  //Origin country, destination country, weight fields are required (weight >=0)           
	if (req.body.weight <= 0 || isNaN(req.body.weight) || req.body.sendercountry == '' || req.body.receivercountry == '')	{
    res.send({"statusGetQuote": false})}

  //Write customer information into the database
  else {
      req.body.weight = convert(req.body.weight).from('kg').to('g');   //Convert weight from kg to g
      MongoClient.connect(url, function(err, db) {
          if (err) throw err;
          var myobj = {
            sendername: `${req.body.sendername}`,
            senderemail: `${req.body.senderemail}`,
            senderphone: `${req.body.senderphone}`,
            senderadress: `${req.body.senderadress}`,
            senderlocality: `${req.body.senderlocality}`,
            senderpostalcode: `${req.body.senderpostalcode}`,
            sendercountry: `${req.body.sendercountry}`,
            receivername: `${req.body.receivername}`,
            receiveremail: `${req.body.receiveremail}`,
            receiverphone: `${req.body.receiverphone}`,
            receiveradress: `${req.body.receiveradress}`,
            receiverlocality: `${req.body.receiverlocality}`,
            receiverpostalcode: `${req.body.receiverpostalcode}`,
            receivercountry: `${req.body.receivercountry}`,
            receiverlength: `${req.body.receiverlength}`,
            receiverwidth: `${req.body.receiverwidth}`,
            receiverheight: `${req.body.receiverheight}`,
            weight: `${req.body.weight}` ,
          };

          //Write customer information into the database
          db.db("ship").collection("user").insert(myobj, function(err, res) {
            if (err) throw err;
            // console.log("Number of documents inserted: " + res.insertedCount);
                                   
            });
          //Find the price based on "rate" table
            db.db("ship").collection("rate").findOne({weight: {$gte:req.body.weight}, from: req.body.sendercountry, to: req.body.receivercountry}, function(err, result) {
                if (err) throw err;

                if (!result) {res.send({"statusGetQuote": false})}
                else {
                pricevalue = result.price
                res.send({'giatien':result.price, "statusGetQuote": true});}
                // console.log(result.weight);
                });
          
    });

  };
  
})


//CreateShipment
app.post('/createShipment', (req,shipment)=>{

var ref = Math.floor((Math.random() * 10000000000)) //Random number in 10 character
  
  //Write shipment information in to the database
  var shipmentobj = {
            ref: `${ref}`,
            'created at': `${new Date().getDate()}/${new Date().getMonth()}/${new Date().getFullYear()} ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`,
            cost: `${pricevalue}`,
            sendername: `${req.body.sendername}`,
            senderemail: `${req.body.senderemail}`,
            senderphone: `${req.body.senderphone}`,
            senderadress: `${req.body.senderadress}`,
            senderlocality: `${req.body.senderlocality}`,
            senderpostalcode: `${req.body.senderpostalcode}`,
            sendercountry: `${req.body.sendercountry}`,
            receivername: `${req.body.receivername}`,
            receiveremail: `${req.body.receiveremail}`,
            receiverphone: `${req.body.receiverphone}`,
            receiveradress: `${req.body.receiveradress}`,
            receiverlocality: `${req.body.receiverlocality}`,
            receiverpostalcode: `${req.body.receiverpostalcode}`,
            receivercountry: `${req.body.receivercountry}`,
            receiverlength: `${req.body.receiverlength}`,
            receiverwidth: `${req.body.receiverwidth}`,
            receiverheight: `${req.body.receiverheight}`,
            weight: `${req.body.weight}` ,

  }
  MongoClient.connect(url, function(err, db) {
  
    db.db("ship").collection("shipment").insert(shipmentobj, function(err, res) {
    if (err) throw err;
    // console.log("Number of documents inserted: " + res.insertedCount);
                           
    });
  
 shipment.send({'ref':ref})})
  });



//GetShipment  
app.post('/getShip', (req,res)=>{
    console.log(req.body);
    MongoClient.connect(url, function(err,db) {
      db.db("ship").collection("shipment").findOne({ref: req.body.trackref}, function(err, result) {
        if (err) throw err;
        if (!result) {return res.send({'statusGetShipment': false})}
        else{
          res.send({"resultGetShip":result,"statusGetShipment": true})}
                // console.log(result.sendername);
        });
    })

    }
    
    );

//DeleteShipment
    app.post('/deleteShip', (req,res)=>{
      console.log(req.body);
      MongoClient.connect(url, function(err,db) {
        db.db("ship").collection("shipment").deleteOne({ref: req.body.trackref}, function(err, result) {
          if (err) throw err;
          res.send(result)
                  // console.log(result.sendername);
          });
      })
  
      }
      
      );
 
  
app.listen(1234, () => { console.log('Running'); });





